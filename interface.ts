//example of simple interface declaration

interface IEmployee {
  empCode: number;
  empName: string;
  getSalary: (arg0: number) => number;
  getManagerName(arg0: number): string;
}

// example of interface as a type

interface NumList {
  [index: number]: number;
}

let numArr: NumList = [1, 2, 3];
numArr[0];
numArr[1];

interface IStringList {
  [index: string]: string;
}
let strArr: IStringList = {};
strArr["TS"] = "Typescript";
strArr["JS"] = "Javascript";

//example optional property

interface IEmployee1 {
  empCode: number;
  empName: string;
  empDebt?: string;
}

let empObj1: IEmployee1 = {
  empCode: 1,
  empName: "Steve",
};

let empObj2: IEmployee1 = {
  empCode: 1,
  empName: "Bill",
  empDebt: "IT",
};

//example : readonly property

interface Citizen {
  name: string;
  readonly SSN: number;
}

let personObj: Citizen = { SSN: 110555444, name: "Jamed bond" };

personObj.name = "Steve Smiith"; //ok
//personObj.SSN = '333666888'; // error because it's a readonly

//example => interface implementation

interface IEmployee {
  empCode: number;
  name: string;
  getSalary: (empCode: number) => number;
}

class Employee2 implements IEmployee1 {
  empCode: number;
  name: string;

  constructor(code: number, name: string) {
    this.empCode = code;
    this.name = name;
  }
  empName: string;
  empDebt?: string | undefined;

  getSalary(empCode: number): number {
    return 20000;
  }
}

//let emp = new Employee2(1, "Steve");

//example class example

class Employer {
  empCode: number;
  empName: string;

  constructor(code: number, name: string) {
    this.empCode = code;
    this.empName = name;
  }
  getSalary(): number {
    return 10000;
  }
}
// example of the build based on closure

// class without constructor

class Employeezn {
  empCd: number;
  empName: string;
}

// example avec an object

class Employerere {
  empCode: number;
  empName: string;
}
let empoweredP = new Employerere();

//example inhheritance
class peson {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
class Employeeee extends Person {
  empCode: string;
}

// example from w3schools

class Personn {
  private name: string;

  public constructor(name: string) {
    this.name = name;
  }

  public gateName(): string {
    return this.name;
  }
}
//const person = new Person("Jane");
//console.log(person.getName()); // c'est pas accessible en dehors du class.

// abstract class

abstract class Polygon {
  public abstract getArea(): number;
  public toString(): string {
    return `Polygon [ area=$[this.getArea()}]`;
  }
}

class Rectangle extends Polygon {
  public constructor(
    protected readonly width: number,
    protected readonly height: number
  ) {
    super();
  }
  public getArea(): number {
    return this.width * this.height;
  }
}
console.log("the width is :");

//

// class with an example of a coded
