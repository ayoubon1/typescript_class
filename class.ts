//  cette partie concernera les classes en typpescript , comment ça fonctionne? et comment les implémenter ?
/* empty classe : une classe vide et basic */

//2
/* fields : we call it a fiels declaration which creates a public writeable property on a class
the problem with this function is the initializer or a method required by typescript to work*/
class Point {
  x: number | undefined;
  y: number | undefined;
}
class Oroboros {
  a: number = 33;
  b: boolean = true;
  c: string = "every day is a new challenge";
  d = [this.a, this.b, this.c];
}

const pt = new Point();
pt.x = 0;
pt.y = 0;
console.log(`${pt.x}, ${pt.y}`);

//3 problème d'initialisation toujours
/* the field needs to be initialized in the constructor itself. a derived class might cause to override methods */
class GoodSalam {
  name: string;
  constructor() {
    this.name = "Hello";
  }
}

//4
/* a method simple to not occur error when initialize a field */
class OkGreeter {
  name!: string;
}

// 5 class contructor
class Pointe {
  x: number;
  y: number;
  //normal signature with defaults
  constructor(x = 1, y = 2) {
    this.x = x;
    this.y = y;
  }
}
class Employee {
  private empCode: number;
  empName: string;
}

let emp = new Employee();
emp.empName = "";

class Person {
  private name: string;
  private age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }
  public GetDetails = {};
}



