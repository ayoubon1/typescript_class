abstract class Cars {
  public description: string;
  public getDescription(): string {
    return this.description;
  }
  public abstract cost(): number;
}

class ModelS1 extends Cars {
  public description = "Model S";

  public cost(): number {
    return 73000;
  }
}

class ModelX1 extends Cars {
  public description = "Model X";
  public cost(): number {
    return 77000;
  }
}

abstract class CarOptions0 extends Cars {
  docoratedCar: Car;
  public abstract getDescription(): string;
  public abstract cost(): number;
}

class EnhanceAutoPilot0 extends CarOptions0 {
  decoratedCar: Car;

  constructor(car: Car) {
    super();
    this.decoratedCar = car;
  }

  public getDescription(): string {
    return this.decoratedCar.getDescription() + ", enhanced copilot";
  }
  public cost(): number {
    return this.decoratedCar.cost() + 5000;
  }
}

/* la demande 2 ème version du code partagé au début du document */
abstract class Car {
  public description: any;
  public getDescription(): string {
    return this.description;
  }
  public abstract cost(): number;
}

class ModelS extends Car {
  public description = "Model S";

  public cost(): number {
    return 73000;
  }
}

class ModelX extends Car {
  public description = "Model X";
  public cost(): number {
    return 77000;
  }
}

abstract class CarOptions extends Car {
  decoratedCar: Car | undefined;
  public abstract getDescription(): string;
  public abstract cost(): number;
}

class EnhanceAutoPilot extends CarOptions {
  decoratedCar: Car;

  constructor(car: Car) {
    super();
    this.decoratedCar = car;
  }

  public getDescription(): string {
    return this.decoratedCar.getDescription() + ", enhanced copilot";
  }
  public cost(): number {
    return this.decoratedCar.cost() + 5000;
  }
}
